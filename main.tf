terraform {
    required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.51.0"
      }
    }
}

provider "aws" {
    region = "eu-central-1"
}

module "Web_resource" {
    source = "./modules/WebResources"
    instance_type = "t2.micro"
    ami = "ami-0453cb7b5f2b7fca2"
}

module "DB_resource" {
    source = "./modules/DBResources"
    DBstorage = "20"
    DBengine = "mariadb"
    DBclass = "db.t2.micro"
    DBname = "demodb"
    DBusername = "root"
    DBpassword = "R00tTest"
}

