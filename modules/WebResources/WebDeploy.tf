## Install Web server commands
data "template_file" "runwebserver" {
  template = <<EOF
    #!/bin/bash
    yum -y update
    yum -y install httpd
    echo "TEST TERRAFORM DEPLOY" > /var/www/html/index.html
    service httpd start
    service httpd on
  EOF
}


## Instances template for Web Servers
    resource "aws_launch_template" "webtier" {
      image_id = var.ami
      instance_type = var.instance_type
     
     #network_interfaces {
     #associate_public_ip_address = false
     #}
    user_data = "${base64encode(data.template_file.runwebserver.rendered)}"
}

## ELB declaration    
resource "aws_elb" "lbinstance" {
    name = "lb-device"
    availability_zones = ["eu-central-1a","eu-central-1b"]

listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
    } 
health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:80/"
    interval = 30
    }

cross_zone_load_balancing = true
idle_timeout = 400
connection_draining = true
connection_draining_timeout = 400
}


output "elb_dns_name" {
   value = aws_elb.lbinstance.dns_name
}

## AutoScale function
    resource "aws_autoscaling_group" "webservers" {
      availability_zones = ["eu-central-1a","eu-central-1b"]
      desired_capacity = 2
      max_size = 2
      min_size = 2
      launch_template {
        id = aws_launch_template.webtier.id
      }
      enabled_metrics = [
        "GroupMinSize",
        "GroupMaxSize",
        "GroupDesiredCapacity",
        "GroupInServiceInstances",
        "GroupTotalInstances"
      ]
      metrics_granularity = "1Minute"

      health_check_type = "ELB"
      load_balancers = [
        aws_elb.lbinstance.id
     ]
      #lifecycle {
      #  create_before_destroy = true
      #}
}
