## Define parameter group
resource "aws_db_parameter_group" "DBParam" {
    name = "mariadb"
    family = "mariadb10.4"
}

## Define basic DB_instance
resource "aws_db_instance" "db" {
    allocated_storage = var.DBstorage
    storage_type = "gp2"
    engine = var.DBengine
    instance_class = var.DBclass
    name = var.DBname
    username = var.DBusername
    password = var.DBpassword
    parameter_group_name = "mariadb"
    skip_final_snapshot = true
}

output "end_point" {
    value = aws_db_instance.db.endpoint
}