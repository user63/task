variable "DBstorage" {
    type = number
    description = "DB storage allocation"
}

variable "DBengine" {
    type = string
    description = "DB engine"
}

variable "DBclass" {
    type = string
    description = "DB class type"
}

variable "DBname" {
    type = string
    description = "DB name"
}

variable "DBusername" {
    type = string
    description = "DB username"
}

variable "DBpassword" {
    type = string
    description = "DB password"
}