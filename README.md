This repository is for TEST and DEMO purposes only.
It requires following software to be installed on your PC
    git
    terraform
    aws cli
Recommanded software:
    Visual studio 

USAGE:
1. Clone this repository 
2. Create aws account (all instances and services are configured to use free tier )
Ref: https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/
3. use aws configure command to config access to your AWS account
Ref: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html
4. Initiate terraform, plan and apply.

Login into AWS console to verify the deployment
Ref: https://console.aws.amazon.com


Notes:
All the parameters deffined are using default VPCs, Subnets, Security Groups and parameters.
For more granularity you may use other repos or customize this one.
This infrastructure deployment is designed to use free tier resources, and does not include payed services.

For additional quiestions, comments: citc5@abv.bg
